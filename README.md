## Exploration des données de Tournesol

Notebook principale : evolution_tournesol_contributions 
Voir Requierment
Les autres notebooks contiennes d'autres explorations.

## Hackathon Tournesol - Data for Good

Le projet consiste à analyser et visualiser les données de la plateforme Tournesol site web de comparaison de vidéos. L'objectif est de produire un tableau de bord qui représente les indicateurs KPI, l'analyse de données utilisateurs, sur les vidéos, sur les comparaisons (votes) et les critères. Les questions qui nous intéressent sont: Le nombre d'utilisateurs du site par semaine, ainsi qu'un tableau indiquant s'il y a des utilisateurs réguliers de longue durée.

En outre, il sera intéressant de suivre le nombre de vidéos comparées au fil du temps et de déterminer combien de vidéos sont comparées par plus d'un ou deux contributeur.rice.s. Cette information peut donner une idée de la diversité des points de vue et de la richesse des discussions sur le site.

Pour visualiser la communauté des contributeur.rices, un bubble plot sera créé où chaque contributeur.rice sera représenté par une bulle dont la taille dépendra du nombre de vidéos comparées ou du nombre de contributions effectuées. Cela permettra de mettre en évidence les contributeur.rice.s les plus actifs et les plus influents sur le site.

Il pourrait être utile de mettre en relation les événements marquants tels que la publication d'une vidéo qui attire beaucoup d'utilisateur.rice.s avec l'utilisation du site. Cette analyse pourrait aider à comprendre l'impact des événements externes sur le site et sur la communauté des contributeur.rices.

En somme, ce projet a pour but d'analyser l'évolution de l'utilisation d'un site de comparaison de vidéos, de mettre en évidence les contributeur.rice.s les plus actifs et les plus influents, et de comprendre l'impact des événements externes sur l'utilisation du site.

## Livrables

Une maquette réactive illustrant une possible implémentation du projet sur le site de Tournesol

Des notebooks documentés contennant les exemples de graphiques analytiques possibles.

## Requierment

pip install plotly (version 5.13.1)
pip install pandas (version 1.5.3)

Changer le chemin d'accès au fichier comparisons.csv

## Note : choses à faire

Rassembler les plot "Cumulative number of compared videos." et "Cumulative number of contributions, all users." sur 1 seul plot.
Cela permettait d'avoir une idée de si les vidéos sont plutôt très comparées entre elles ou si au contraire les comparaisons sont très éparses.

## Auteurs

- Adrien Pacifico
- Romain Attié
- Wandrille Legras
- Angela Drucioc
